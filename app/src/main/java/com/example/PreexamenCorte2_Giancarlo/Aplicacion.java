package com.example.PreexamenCorte2_Giancarlo;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.UsuariosDb;

public class Aplicacion extends Application {

    public static ArrayList<Usuarios> usuarios;
    private MiAdaptador MiAdaptador;
    private UsuariosDb usuarioDb;

    public ArrayList<Usuarios> getUsuarios() {
        return usuarios;
    }
    public MiAdaptador getAdaptador() {
        return MiAdaptador;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        usuarioDb = new UsuariosDb(getApplicationContext());
        usuarios = usuarioDb.allUsuarios();
        usuarioDb.openDataBase();
        //Muestre un aviso en la terminal que si esta funcionando
        Log.d("", "onCreate: Tamaño array list: " + this.usuarios.size());
        this.MiAdaptador = new MiAdaptador(this.usuarios, this);
    }

    public void agregarUsuario(Usuarios usuario) {
        usuarioDb.openDataBase();
        long resultado = usuarioDb.insertUsuario(usuario);
        if (resultado != -1) {
            //Si el resultado fue exitoso va añadir al usuario nuevo
            usuario.setId((int) resultado);
            usuarios.add(usuario);
            //para que muestre todo lo que haya
            MiAdaptador.notifyDataSetChanged();
        }
    }
}
