package com.example.PreexamenCorte2_Giancarlo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import Modelo.UsuariosDb;

public class UsuarioActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton btnRegresar;
    private Aplicacion app;
    private Usuarios usuario;
    private int posicion;
    private UsuariosDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        this.inicializarComponentes();
        this.btnRegresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) { btnRegresar(); }
        });
        app.getAdaptador().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { recyclerView(view); }
        });
    }

    private void inicializarComponentes(){
        this.usuario = new Usuarios();
        this.posicion = -1;
        this.app = (Aplicacion) getApplication();
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView = findViewById(R.id.recId);
        this.recyclerView.setAdapter(app.getAdaptador());
        this.recyclerView.setLayoutManager(this.layoutManager);

        this.btnRegresar = findViewById(R.id.btnRegresar);
        this.usuarioDb = new UsuariosDb(getApplicationContext());

    }

    private void btnRegresar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmación");
        builder.setMessage("¿Estás seguro de que quieres regresar?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Regresar al MainActivity
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void recyclerView(View view) {
        posicion = recyclerView.getChildAdapterPosition(view);
        usuario = app.getUsuarios().get(posicion);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == Activity.RESULT_OK) {
            app.getUsuarios().clear();
            app.getUsuarios().addAll(usuarioDb.allUsuarios());
            app.getAdaptador().setUsuarioList(app.getUsuarios());
            recreate();
            app.getAdaptador().setUsuarioList(app.getUsuarios());
        }

        this.posicion = -1;
        this.usuario = null;
    }

}