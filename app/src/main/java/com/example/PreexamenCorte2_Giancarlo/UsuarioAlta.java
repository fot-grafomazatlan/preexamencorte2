package com.example.PreexamenCorte2_Giancarlo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.UsuariosDb;

public class UsuarioAlta extends AppCompatActivity {
    private EditText txtNombreUsuario, txtCorreo, txtContrasena, txtReContrasena;
    private Button btnRegistrar, btnIngresar;
    private UsuariosDb usuarioDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_alta);

        inicializarComponentes();

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRegistrar();
            }
        });
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnIngresar();
            }
        });
    }

    private void inicializarComponentes() {
        txtNombreUsuario = findViewById(R.id.txtNombreUsuario);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtContrasena = findViewById(R.id.txtContraseña);
        txtReContrasena = findViewById(R.id.txtConfirmarContraseña);
        btnRegistrar = findViewById(R.id.btnGuardar);
        btnIngresar = findViewById(R.id.btnRegresar);
        usuarioDb = new UsuariosDb(getApplicationContext());
    }

    private void btnRegistrar() {
        String nombreUsuario = txtNombreUsuario.getText().toString();
        String correo = txtCorreo.getText().toString();
        String contrasena = txtContrasena.getText().toString();
        String reContrasena = txtReContrasena.getText().toString();

        // Validar que los campos no estén vacíos
        if (nombreUsuario.isEmpty() || correo.isEmpty() || contrasena.isEmpty() || reContrasena.isEmpty()) {
            Toast.makeText(UsuarioAlta.this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        // Validar que las contraseñas coincidan
        if (!contrasena.equals(reContrasena)) {
            Toast.makeText(UsuarioAlta.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            return;
        }


        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(correo).matches()) {
            Toast.makeText(UsuarioAlta.this, "Ingrese un correo electrónico válido", Toast.LENGTH_SHORT).show();
            return;
        }


        Usuarios usuarioExistente = usuarioDb.getUsuario(correo);
        if (usuarioExistente != null) {
            Toast.makeText(UsuarioAlta.this, "Ya existe un usuario con este correo.", Toast.LENGTH_SHORT).show();
            return;
        }


        // Crear un nuevo usuario
        Usuarios nuevoUsuario = new Usuarios(nombreUsuario, correo, contrasena);

        // Insertar el nuevo usuario en la base de datos
        long resultado = usuarioDb.insertUsuario(nuevoUsuario);
        if (resultado != -1) {
            Toast.makeText(UsuarioAlta.this, "Registro exitoso bienvenido "+nombreUsuario, Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(UsuarioAlta.this, "Error al registrar el usuario", Toast.LENGTH_SHORT).show();
        }

        Aplicacion app = (Aplicacion) getApplication();
        app.getUsuarios().add(nuevoUsuario);
    }

    private void btnIngresar(){
        finish();
    }

}
