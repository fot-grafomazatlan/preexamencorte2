package Modelo;



import android.database.Cursor;

import com.example.PreexamenCorte2_Giancarlo.Usuarios;

import java.util.ArrayList;

public interface Proyeccion {
    Usuarios getUsuario(String nombreUsuario);
    ArrayList<Usuarios> allUsuarios();
    Usuarios readUsuario(Cursor cursor);
}