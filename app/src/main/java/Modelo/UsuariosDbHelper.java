package Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UsuariosDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMAT_SEP = " ,";
    private static final String SQL_CREATE_USUARIO = "CREATE TABLE " +
            DefineTabla.Usuarios.TABLE_NAME + " (" +
            DefineTabla.Usuarios.COLUMN_NAME_ID + INTEGER_TYPE + " PRIMARY KEY, " +
            DefineTabla.Usuarios.COLUMN_NAME_NOMBRE_USUARIO + TEXT_TYPE + COMMAT_SEP +
            DefineTabla.Usuarios.COLUMN_NAME_CORREO + TEXT_TYPE + COMMAT_SEP +
            DefineTabla.Usuarios.COLUMN_NAME_CONTRASENA + TEXT_TYPE + ")";
    private static final String SQL_DELETE_USUARIO = "DROP TABLE IF EXISTS " +
            DefineTabla.Usuarios.TABLE_NAME;
    private static final String DATABASE_NAME = "sistema9.db";
    private static final int DATABASE_VERSION = 2;

    public UsuariosDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USUARIO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_USUARIO);
        onCreate(db);
    }
}