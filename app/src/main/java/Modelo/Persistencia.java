package Modelo;

import com.example.PreexamenCorte2_Giancarlo.Usuarios;

public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertUsuario(Usuarios usuario);

}
